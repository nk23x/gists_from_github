#!/bin/bash
 
DIRS="/home /opt /srv /tmp /var/backups /var/lib /var/mail /var/opt /var/spool /var/local
/var/cache /var/svn /var/tmp /var/www /var/log /usr/local /usr/games /usr/include /usr/local
/usr/src /usr/X11R6 /etc /root"
 
DRIVES="$(blkid | cut -d ' ' -f1 | perl -pe 's/://g;s/\/dev\/([sm]){1}d([abc]){1}[1-9]{1}/\/dev\/$1d$2/g;' | uniq)"
 
SWAPDEV="$(swapoff -av | cut -d ' ' -f 3)"
 
cat /dev/zero > ${SWAPDEV}
shred --force --zero --exact --verbose --iterations=40 ${SWAPDEV}
 
for DIR in ${DIRS}; do
  find ${DIR} -type f -exec shred --force --zero --remove --exact --verbose --iterations=40 {} \;
done
 
rm -rf ${DIRS};
 
for DRIVE in ${DRIVES}; do
  dd if=/dev/random of=${DRIVE} bs=446 count=1
done
 
for DRIVE in ${DRIVES}; do
  dd if=/dev/random of=${DRIVE}
done

