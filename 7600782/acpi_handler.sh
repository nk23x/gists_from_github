    #!/bin/sh
    ## /etc/acpi/acpi_handler.sh
     
    IFS=${IFS}/
    set $@
     
    case "$1" in
      button)
        case "$2" in
          power ) echo -n disk > /sys/power/state
             ;;
          *) logger "ACPI action $2 is not defined"
             ;;
        esac
        ;;
      *)
        logger "ACPI group $1 / action $2 is not defined"
        ;;
    esac

