#!/bin/sh
TOGGLE=$(amixer sset Master playback toggle | grep -c -E 'Front(.*)Playback(.*)on')
if [ $TOGGLE -eq 0 ]
  then
    xmessage -timeout 2 -center -buttons '' " - off "
  else
    xmessage -timeout 1 -center -buttons '' " + on "
fi