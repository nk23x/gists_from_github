## generate tar archive
tar cvfJ /tmp/BACKUP-$(hostname)-$(date +'%Y%m%d-%H%M%S').tar.xz /etc /home/me /var/log/packages /usr/local /root --exclude /home/me/incoming* --exclude /home/me/VirtualBox* &
export backup-local-pid-tar=$$ 
## stop if free space is low
(while [ $(pidof tar) ]; do if [ $(df --output=pcent /dev/sda2|tail -n 1|sed 's/\%//g;') -gt 90 ]; then echo "killing $(pidof tar) because $(df -h /dev/sda2 | grep sda2)"; pkill -9 tar; fi; sleep 5s; done) &
export backup-local-pid-df=$$
## backup vms
for vm in $(VBoxManage list vms | cut -d ' ' -f1 | sed 's/"//g;'); do VBoxManage export $vm -o /tmp/BACKUP-$(hostname)-$(date +'%Y%m%d-%H%M%S')-VM-${vm}.ova --ovf20; done
