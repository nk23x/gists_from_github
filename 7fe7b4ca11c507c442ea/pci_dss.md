PCI DSS
=======
NOTE: Work in progress

TODO: Identity management, two-factor auth, OpenVPN, Logstash, log shippers, IIS logs, OSSEC, Snort, Suricata, snorby, restart iis w/o admin role, 

PCI DSS. Guidelines
-------------------
- PCI Compliance with Open Source at (Almost) Zero Cost. Part 1 - http://rafeeqrehman.com/2011/05/24/zero-cost-pci-compliance/
- PCI Compliance with Open Source at (Almost) Zero Cost. Part 2 - http://rafeeqrehman.com/2011/07/17/zero-cost-pci-compliance-part-2/
- PCI compliance with Linux and Open Source at Ohio Linux Fest - http://rafeeqrehman.com/wp-content/uploads/2011/09/ohio-linux-fest.pdf
 
 
- PCI DSS and Red Hat Enterprise Linux. Part #1 - http://blog.ptsecurity.com/2010/07/red-card-specificity-of-pci-dss-in.html
- PCI DSS and Red Hat Enterprise Linux. Part #2 - http://blog.ptsecurity.com/2010/07/red-card-specificity-of-pci-dss-in_19.html
- PCI DSS and Red Hat Enterprise Linux. Part #3 - http://blog.ptsecurity.com/2010/08/pci-dss-and-red-hat-enterprise-linux.html
- PCI DSS and Red Hat Enterprise Linux. Part #4 - http://blog.ptsecurity.com/2010/09/pci-dss-and-red-hat-enterprise-linux.html
- PCI DSS and Red Hat Enterprise Linux. Part #5 - http://blog.ptsecurity.com/2010/09/pci-dss-and-red-hat-enterprise-linux_03.html
- PCI DSS and Red Hat Enterprise Linux. Part #6 - http://blog.ptsecurity.com/2010/10/pci-dss-and-red-hat-enterprise-linux.html
- PCI DSS and Red Hat Enterprise Linux. Part #7 - http://blog.ptsecurity.com/2010/10/pci-dss-and-red-hat-enterprise-linux_20.html
- PCI DSS and Red Hat Enterprise Linux. Part #8 - http://blog.ptsecurity.com/2010/11/requirement-10-track-and-monitor-all.html
- PCI DSS and Red Hat Enterprise Linux. Part #9 - http://blog.ptsecurity.com/2010/11/pci-dss-and-red-hat-enterprise-linux.html
 
 
- REQ 2, 3, 4, 12, 13. Encryption basics - http://pciguru.wordpress.com/2012/01/01/encryption-basics/
- REQ 3.5. Encryption key management primer - http://pciguru.wordpress.com/2012/01/15/encryption-key-management-primer-requirement-3-5/
- REQ 3.6. Encryption key management primer - http://pciguru.wordpress.com/2012/01/28/encryption-key-management-primer-requirement-3-6/
- REQ 6.1. Intent of requirements - http://pciguru.wordpress.com/2011/01/27/intent-of-requirements-%E2%80%93-6-1/
- REQ 6.6. Part 1 - http://pciguru.wordpress.com/2009/03/01/requirement-66-%E2%80%93-the-misunderstood-requirement-%E2%80%93-part-1/
- REQ 6.6. Part 2 - http://pciguru.wordpress.com/2009/03/02/requirement-66-%E2%80%93-part-2/
- REQ 6.6. Part 3 - http://pciguru.wordpress.com/2009/03/03/requirement-66-%E2%80%93-part-3/
- REQ 8.5.1. The flaw in requirement - http://pciguru.wordpress.com/2014/07/01/the-flaw-in-requirement-8-5-1/
- REQ 9.7.1. Annoying requirements - http://pciguru.wordpress.com/2010/05/13/annoying-requirements-%E2%80%93-9-7-1/
- REQ 10.6. PCI DSS v3 - http://pciguru.wordpress.com/2014/01/25/pci-dss-v3-requirement-10-6/
- REQ 11.2. Intent of requirements - http://pciguru.wordpress.com/2011/02/03/intent-of-requirements-%E2%80%93-11-2/
- Adventures In Finding Cardholder Data - http://pciguru.wordpress.com/2014/05/18/adventures-in-finding-cardholder-data/
- Pre-Authorization Data - http://pciguru.wordpress.com/2014/02/08/pre-authorization-data-2/
- An Open Letter To Executives - http://pciguru.wordpress.com/2014/04/13/an-open-letter-to-executives/
 
 
- REQ 3.4.1. Disk Encryption - http://blog.403labs.com/post/8402704243/disk-encryption-and-pci-requirement-3-4-1
- REQ 6.5. App Development and OWASP Top 10. Part 1 - http://blog.403labs.com/post/20466128184/secure-application-development-and-the-owasp-top-10
- REQ 6.5. App Development and OWASP Top 10. Part 2 - http://blog.403labs.com/post/24211952692/secure-application-development-and-the-owasp-top-10
- REQ 6.5. App Development and OWASP Top 10. Part 2 - http://blog.403labs.com/post/31924845591/secure-application-development-and-the-owasp-top-10
 
 
- PCI DSS. The Open Source Way - http://www.qcode.co.uk/pci-dss-compliance-the-open-source-way/index.html
- REQ 8.1. Two-factor Authentication - http://www.qcode.co.uk/pci-dss-requirement-8-part-1-two-factor-authentication/
- REQ 8.2. Stunnel & Plain Text Passwords - http://www.qcode.co.uk/pci-dss-requirement-8-part-2-stunnel-plain-text-passwords/
- REQ 8.3. User & Password Policy - http://www.qcode.co.uk/pci-dss-requirement-8-part-3-user-password-policy/
- REQ 10.1. Logging with Rootsh - http://www.qcode.co.uk/pci-dss-requirement-10-part1-logging-with-rootsh/
- REQ 10.2. NTP Time Synchronisation - http://www.qcode.co.uk/pci-dss-requirement-10-part-2-ntp-time-synchronisation/
- REQ 10.3. Centralised Logging - http://www.qcode.co.uk/pci-dss-requirement-10-part-3-centralised-logging/
- REQ 10.4. Log File Monitoring (and more) with OSSEC - http://www.qcode.co.uk/pci-dss-requirement-10-part-4-log-file-monitoring-and-more-with-ossec/
 
 
- PCI DSS совместимость. Часть 1 - http://www.winsecurity.ru/articles/pci-dss-compliance.html
- PCI DSS совместимость. Часть 2 - http://www.winsecurity.ru/articles/pci-dss-compliance-part2.html
 
REQ 10.1, 10.2. Tools. Resource access tracking
-----------------------------------------------
- auditd - Linux audit daemon - http://linux.die.net/man/8/auditd
- rootsh - a logging wrapper for shells - http://linux.die.net/man/1/rootsh
- grsecurity - http://grsecurity.net/
- Process Accounting - http://www.tldp.org/HOWTO/Process-Accounting/
 
REQ 10.1, 10.2. Tools. Resource access tracking. auditd
-------------------------------------------------------
- A Brief Introduction to auditd - http://security.blogoverflow.com/2013/01/a-brief-introduction-to-auditd/
- Stump the Chump with Auditd 01 - http://security.blogoverflow.com/2013/09/stump-the-chump-with-auditd-01/
- Auditd in Linux for PCI DSS compliance - http://networkrecipes.blogspot.com/2013/03/auditd-in-linux-for-pci-dss-compliance.html
- The Linux Audit System, or Who Changed That File? - http://www.la-samhna.de/library/audit.html
- Convert Linux uid / gid into user / group names - http://www.linuxquestions.org/questions/linux-security-4/auditd-audit-log-not-display-date-or-user-643815/#post3166168 
 
REQ 10.1, 10.2. Tools. Resource access tracking. rootsh
-------------------------------------------------------
- Regarding rootsh - http://ubuntuforums.org/showthread.php?t=1764085
- How can I log user activity on SSH - http://serverfault.com/questions/362532/how-can-i-log-user-activity-on-ssh
- Log SSH activity - http://askubuntu.com/questions/112686/log-ssh-activity
- rootsh terminal logger keeps watch on root users - http://archive09.linux.com/feature/61687
 
REQ 10.3. Tools. Log management
-------------------------------
- syslog-ng - Multiplatform Syslog Server and Logging Daemon - http://www.balabit.com/network-security/syslog-ng/
- logstash - open source log management - http://logstash.net/
- logstash-forwarder - https://github.com/elasticsearch/logstash-forwarder
- nxlog - open-source multi-platform log management - http://nxlog.org/
- Graylog2 - Open source log management and data analytics - http://graylog2.org/
- Fluentd - Open Source Log Management - http://www.fluentd.org/
- Apache Flume - collecting, aggregating, and moving large amounts of log data - http://flume.apache.org/
- Kibana - http://www.elasticsearch.org/overview/kibana/

REQ 10.3. Tools. Log management. nxlog
--------------------------------------
- Python log tailer script could be user as input - http://nxlog.org/nxlog-docs/en/nxlog-reference-manual.html#im_exec
- Perl function can be called for each log event - http://nxlog.org/nxlog-docs/en/nxlog-reference-manual.html#xm_perl
- pm_transformer module can convert syslog to json - http://nxlog.org/nxlog-docs/en/nxlog-reference-manual.html#pm_transformer
 
PCI DSS. Tools. Vulnerability management
----------------------------------------
- Nessus - http://www.nessus.org
- Nmap - http://www.nmap.org
- Kismet Wireless detection and sniffing - http://www.kismetwireless.net/
- Backtrack - http://www.remote-exploit.org/backtrack.html
- Web application testing with w3af    
- OpenVAS Vulnerability Scanner (is like Nessus client/Server) - http://www.openvas.org/
- SSL crypto verification and certificate checking - SSLscan, available on Linux
 
PCI DSS. Tools. Penetration testing
-----------------------------------
- Metasploit - http://www.metasploit.com/
- Backtrack - http://www.remote%C2%AD%E2%80%90exploit.org/backtrack.html
- Wireshark packet capture and analysis - http://www.wireshark.org/
 
PCI DSS. Python apps
--------------------
- Building PCI compliant Django applications - https://speakerdeck.com/kencochrane/building-pci-compliant-django-applications