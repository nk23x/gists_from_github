# STATUS

## OVERVIEW / IOE / ATTACKS
* http://map.norsecorp.com/#/
* http://www.sicherheitstacho.eu/
* http://www.digitalattackmap.com/#anim=1&color=0&country=ALL&list=0&time=17023&view=map
* https://threatmap.checkpoint.com/ThreatPortal/livemap.html
* https://www.fireeye.com/cyber-map/threat-map.html
* https://threatmap.fortiguard.com/
* http://atlas.arbor.net/
* http://www.trendmicro.com/us/security-intelligence/current-threat-activity/global-botnet-map/index.html
* http://globalsecuritymap.com/
* http://www.cyren.com/security-center-new.html#dashboard
* http://www.cyren.com/security-center-new.html#map
* https://www.akamai.com/de/de/solutions/intelligent-platform/visualizing-akamai/real-time-web-monitor.jsp

## ISPs

### GERMANY
* http://online-status.net/status.php

## CLOUD

### GOOGLE
* https://status.cloud.google.com/
* https://status.cloud.google.com/summary 

### AWS
* https://status.aws.amazon.com/

## SERVICES

### GITHUB
* https://status.github.com/

## CDN

### CLOUDFLARE
* https://www.cloudflarestatus.com/ 

### AKAMAI
* https://www.akamai.com/us/en/solutions/intelligent-platform/visualizing-akamai/real-time-web-monitor.jsp
* http://wwwnui.akamai.com/gnet/globe/index.html

