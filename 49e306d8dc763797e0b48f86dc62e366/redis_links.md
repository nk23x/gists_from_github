articles on redis:
* https://www.packtpub.com/mapt/book/big_data_and_business_intelligence/9781783286713/5
* https://www.rtinsights.com/ecommerce-latency-redis-case-study/
* https://medium.com/@RedisLabs/high-speed-transactions-for-an-on-demand-world-f8452a46ea0d
* https://stackoverflow.com/questions/49063032/redis-suitable-data-structure-ecommerce-usecase

redislabs:
* https://redislabs.com/solutions/ecommerce/
* https://redislabs.com/solutions/industries/redis-for-retail-and-e-commerce/   

alternative software:
* hazelcast
  * https://www.one-tab.com/page/V2WM4mPcQki0IY21U9Wf3g