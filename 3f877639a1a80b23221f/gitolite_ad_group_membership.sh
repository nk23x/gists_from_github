#!/bin/sh
set -e
# Get a list of groups a user is a member of on one line, space-seperated
# Single-quote group names with spaces, otherwise print the rest

SPACE_CHAR='-'

TMP=`mktemp`
ldapsearch -H ldap://domain.local -b OU=Everything,DC=domain,DC=local -LLL -x -z0 -D 'CN=gitolite,OU=Engineering,OU=Everything,DC=domain,DC=local' -y /var/lib/git/gitolite_ad_passwd "(sAMAccountName=$1)" userAccountControl memberOf > "$TMP"

# Is the account still valid? non-zero = no, zero = yes
awk '{if(!and($2,0x02)){print $0}}' "$TMP" \
 | grep -qse 'userA' - || {
  shred -u "$TMP"
  false
} 

awk '/^ /{x=$0;gsub(" ","",x);print x};!/^ /{if(length($0)==78){printf$0}else{print}}' "$TMP" | \
 grep -e 'memberOf: ' | \
 sed 's/.*CN=\([^,]*\),.*/\1/g' | \
 tr ' \n' "$SPACE_CHAR " || { 
  shred -u "$TMP"
  false
}
# Bit 1 (decimal value 2) of userAccountControl : 1 = account disabled, 0 = account enabled
