#!/bin/bash
SRC_ORIG="$1"
SRC_NEW="$2"
PATCH_NAME="$3"

if [[ -z $3 ]]; then 
  echo -e "\n error! syntax is:\n\n $0 ORIGINAL MODIFIED PATCH_NAME\n"
  exit 
fi

if [[ -d $SRC_ORIG ]] && [[ -d $SRC_NEW ]]; then 
  INTYPE="DIR"
elif [[ -f $SRC_ORIG ]] && [[ -f $SRC_NEW ]]; then 
  INTYPE="FILE"
else 
  echo -e "\n error!\n\n use either directories or files to create a patch.\n"
  exit 
fi

if [[ $INTYPE == "FILE" ]]; then 
  diff -Naru $SRC_ORIG $SRC_NEW > $PATCH_NAME 
elif [[ $INTYPE == "DIR" ]]; then  
  diff -crB $SRC_ORIG $SRC_NEW > $PATCH_NAME
fi
