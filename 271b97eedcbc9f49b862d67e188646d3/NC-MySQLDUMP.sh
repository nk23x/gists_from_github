#####
# You'll be needing two machines, the target machine and source one (makes sense, right)?

#####
# On the target machine
nc -l 55555 | gzip -d -c | mysql <database name> -u<user> -p<password> [ | <decrypt> ]

#####
# On the source machine
mysqldump -u<user> -p<password> <database name> | gzip | nc <ip of target server> 55555 [ | <encrypt> ]

#####
# Adding Encryption...

# Encrypting with openssl
/usr/bin/openssl enc -aes-256-cbc -pass pass:<some_password> -e

# DECRYPTING THE FILE
# /usr/bin/openssl enc -aes-256-cbc -pass pass:<some_password> -d