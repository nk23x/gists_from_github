#!/system/xbin/ash

echo "serving /dev/block/mmcblk0 on :20099"

nc -ll -p 20099 -e dd if=/dev/block/mmcblk0 &

echo ""
echo "serving /dev/block/mmcblk1 on :20098"

nc -ll -p 20098 -e dd if=/dev/block/mmcblk1 &

echo "pids: $(pidof nc)"



