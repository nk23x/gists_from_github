#!/bin/bash                                                                                                             
                                                                                                                        
## this gives you a list of links to download the latest releases                                                       
## + filter links to chrome beta an development versions                                                                
## + skip sf download page                                                                                              
                                                                                                                        
LINKLIST=$(cat <<LLIST                                                                                                  
http://portableapps.com/apps/development/notepadpp_portable                                                             
http://portableapps.com/apps/graphics_pictures/cornice_portable                                                         
http://portableapps.com/apps/graphics_pictures/gimp_portable                                                            
http://portableapps.com/apps/graphics_pictures/xnview_portable                                                          
http://portableapps.com/apps/internet/google_chrome_portable                                                            
http://portableapps.com/apps/internet/kitty-portable                                                                    
http://portableapps.com/apps/internet/links-portable                                                                    
http://portableapps.com/apps/internet/seamonkey_portable                                                                
http://portableapps.com/apps/internet/skype_portable                                                                    
http://portableapps.com/apps/internet/winscp_portable                                                                   
http://portableapps.com/apps/music_video/mpc-hc-portable                                                                
http://portableapps.com/apps/office/sumatra_pdf_portable                                                                
http://portableapps.com/apps/security/clamwin_portable                                                                  
http://portableapps.com/apps/security/gpg-plugin-portable                                                               
http://portableapps.com/apps/security/kaspersky-tdsskiller-portable                                                     
http://portableapps.com/apps/security/mcafee-stinger-portable                                                           
http://portableapps.com/apps/security/spybot_portable                                                                   
http://portableapps.com/apps/utilities/7-zip_portable                                                                   
http://portableapps.com/apps/utilities/autoruns-portable                                                                
http://portableapps.com/apps/utilities/command_prompt_portable                                                          
http://portableapps.com/apps/utilities/console_portable                                                                 
http://portableapps.com/apps/utilities/explorerplusplus_portable                                                        
http://portableapps.com/apps/utilities/fastcopy-portable                                                                
http://portableapps.com/apps/utilities/hwinfo-portable                                                                  
http://portableapps.com/apps/utilities/jkdefrag_portable                                                                
http://portableapps.com/apps/utilities/peazip_portable                                                                  
http://portableapps.com/apps/utilities/process-monitor-portable                                                         
http://portableapps.com/apps/utilities/teamviewer_portable                                                              
http://portableapps.com/apps/utilities/ultradefrag-portable                                                             
http://portableapps.com/apps/utilities/winmerge_portable                                                                
http://portableapps.com/apps/utilities/xenon_portable                                                                   
http://portableapps.com/apps/utilities/xpy-portable                                                                     
LLIST                                                                                                                   
)                                                                                                                       

for i in $(echo $LINKLIST)                                                                                              
 do                                                                                                                     
   wget -q -O - $i | \                                                                                                  
   grep 'sourceforge_accelerator_link' | \                                                                              
   perl -pe 's/^.*href=\"(.*)\" class=\"sourceforge_accelerator_link.*/$1/g;' | \                                       
   perl -pe 's/^(.*)sourceforge(.*)\/download/$1sourceforge$2\?r=&ts=1441447350&use_mirror=netcologne/g;' | \           
   grep -v -E 'Chrome.*Beta|Chrome.*Dev'                                                                                
done                                                                                                                    
                                                                                                                        
exit 0                                                                                                                  
                                                                                                                                                                                                                                                
                                         