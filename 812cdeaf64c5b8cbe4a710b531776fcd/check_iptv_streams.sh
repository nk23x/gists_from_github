for u in $(grep -E '^(http|rtmp|rtsp):' -h /tmp/PLAYLISTS/*); do curl --connect-timeout 3 -s -o /dev/null -w "%{http_code} %{url_effective} %{content_type} %{time_connect} %{time_total}\n" $u | grep -E '^200 '>> /tmp/iptv_working.txt; done 

##
## outputs a text file /tmp/iptv_working.txt with entries like 
##
## 200 rtsp://rtmp.infomaniak.ch/livecast/barntele  0,164 0,216
## 200 rtsp://payamlive.nanocdn.com:1935/live/_definst_/payam256  0,212 0,288
## 200 rtsp://media19.internet.gr/ParliamentTV-HQ  0,206 0,593
## 200 rtsp://madhatv.cherritech.us:1935/madhatv/myStream  0,390 0,667
##
## which can be parsed using cut
##
## for s in $(cat /tmp/iptv_working.txt | cut -d ' ' -f 2); do ffplay "${s}"; sleep 1; done
##