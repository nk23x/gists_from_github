#/bin/bash

if [ "$USER" != "root" ]
 then 
   echo "please become root to exec this script"
   exit 0
fi

AKKU_NAME="$(dmidecode | grep -A 10 Battery | grep -E 'Name' | sed s/'.*Name: '//)"

if [[ "$(echo $AKKU_NAME | grep -c 424T)" = "1" ]]
  then
    for s in 695 711 740 798 804 812 816 822 826 828 834 840 862 868 874 880 890 944 948 954 958
      do 
        if [[ "$(echo $AKKU_NAME | grep $s)" ]]
          then 
            echo "YOUR AKKU IS AFFECTED!"
            echo "please sent the following Info to sschwalbe@notebooksbilliger.de"
            echo 
            
            dmidecode | grep -A 12 Battery | grep -E 'Name|Serial|Date'   
            dmidecode | grep -A 8 'System Information' | grep -E 'Serial Number|Version|Product' | sort -r
            
            echo
        fi
      done
fi

if [[ "$(echo $AKKU_NAME | grep -c 45N10)" = "1" ]] 
  then 
    for s in 22 50 
      do 
        if [[ "$(echo $AKKU_NAME | grep $s)" ]]
          then 
            echo "YOUR AKKU IS AFFECTED!"
            echo "please sent the following Info to sschwalbe@notebooksbilliger.de"
            echo 
            
            dmidecode | grep -A 12 Battery | grep -E 'Name|Serial|Date'   
            dmidecode | grep -A 8 'System Information' | grep -E 'Serial Number|Version|Product' | sort -r
            
            echo
        fi
     done
fi

