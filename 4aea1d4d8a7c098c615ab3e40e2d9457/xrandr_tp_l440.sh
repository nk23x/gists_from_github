#!/bin/bash

## set parameters for connected displays:
OUT_ROTATE="--rotate normal"
OUT_EDP0="--mode 1600x900 --pos 0x0"
OUT_HDMI0="--primary --mode 1920x1080 --pos 3520x0"
OUT_VGA0="--mode 1920x1080 --pos 1600x0"

##
##   xrandr -q | cut -d ' ' -f1,2 | grep -vE '^( |Screen)')
##
## returns something like
## 
##   eDP-0 connected
##   VGA-0 disconnected
##   DisplayPort-0 disconnected
##   HDMI-0 disconnected
##   DisplayPort-1 disconnected
##   HDMI-1 disconnected
##

## count connected outputs
CONNECTED_OUTPUTS_COUNT=$(xrandr -q | cut -d ' ' -f1,2 | grep -vE '^( |Screen)' | grep -c ' connected')

## if more than one display is connected exec xrandr
if [ $CONNECTED_OUTPUTS_COUNT -gt '1' ]; then 
  xrandr --output eDP-0 $OUT_EDP0 $OUT_ROTATE --output HDMI-0 $OUT_HDMI0 $OUT_ROTATE --output VGA-0 $OUT_VGA0 $OUT_ROTATE
fi
