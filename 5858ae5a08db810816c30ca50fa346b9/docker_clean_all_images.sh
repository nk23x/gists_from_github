#!/bin/bash

if [ "$(echo $USERNAME)" != "root" ]; then 	
  echo "this script needs to be run by root"	
  exit 1
 fi

if 

case "${1}" in    

  'images')
    while $(docker images -all --no-trunc | grep -vE '^[A-Z].*'); do 	
      for image in $(docker images --all --no-trunc | grep -vE '^[A-Z].*' | perl -pe 's/.*sha256:([0-9a-z]{1,})\s+ [0-9]{1}.*/$1/g;'); do
        docker rmi --force $image 
      done 
    done
   ;;

  'containers')
    for container in $(docker ps --all --no-trunc --size | grep -vE '^[A-Z].*' | perl -pe 's/^([0-9a-z]{1,})\s+ [0-9]{1}.*/$1/g;'); do
      docker rm --volumes --force $container
    done
  ;;

esac
exit 0
