#!/bin/bash

if [ "$(echo $USERNAME)" != "root" ]; then 
	echo "this script needs to be run by root"
	exit 1
fi

DOCKER_VERSION=$(docker version| grep -m 1 'Version' | perl -pe 's/.* ([1-9]{2})..*/$1/g;')

if [ "$DOCKER_VERSION" -gt  "16" ]; then
	docker system prune --all --force	
else
	while $(docker images -a --no-trunc | grep -v IMAGE); do 
		for image in $(docker images -a --no-trunc | perl -pe 's/\s+ / /g;' | cut -d ' ' -f 3 | grep -v IMAGE); do 
			docker rmi -f --no-prune $image 
		done 
	done
fi
