#!/bin/bash
SLACKWARE_VERSION="$(cut -d ' ' -f 2 /etc/slackware-version)"

slackpkg -batch=on -default_answer=y install lftp
RUN lftp -c 'open http://slackware.com/~alien/multilib/ ; mirror -c -e $()$SLACKWARE_VERSION'

RUN upgradepkg --reinstall --install-new ./$SLACKWARE_VERSION/*.t?z
RUN upgradepkg --install-new ./$SLACKWARE_VERSION/slackware64-compat32/*-compat32/*.t?z

