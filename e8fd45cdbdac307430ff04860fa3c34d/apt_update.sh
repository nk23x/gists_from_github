#!/bin/bash

if [ "$(echo $USERNAME)" != "root" ]; then 	
  echo "this script needs to be run by root"	
  exit 1
fi

if [ -f /etc/debian_version ]; then
  if [ -x $(which apt-get) ]; then
    for task in update "upgrade -y -m" clean autoclean; do
      apt-get $task -qq; 		
    done	
  fi
fi

exit 0
