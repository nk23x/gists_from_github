wget -N -q -c $(echo "http://cdimage.debian.org/debian-cd/current/amd64/iso-cd/$(wget -q -O - http://cdimage.debian.org/debian-cd/current/amd64/iso-cd | grep '\-amd64-netinst.iso' | grep -v 'debian-mac' | perl -pe 's/.*.iso\">(.*)<.*/$1/g;')") \
&& rm debian-LATEST-amd64-netinst.iso \
&& ln -s $(ls -1 -t -r *-amd64-netinst.iso | tail -n 1) debian-LATEST-amd64-netinst.iso

## even if the download is not necessary, the old symlink will be recreated. 
## it's not sexy but it works fine and without overhead ;)