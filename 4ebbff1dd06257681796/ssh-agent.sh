# Some ssh-agent magic
export SSH_AUTH_SOCK=/tmp/.ssh-socket

ssh-add -l >/dev/null 2>&1

if [ $? = 2 ]; then
    # Exit status 2 means couldn't connect to ssh-agent; start one now
    rm -rf /tmp/.ssh-*
    ssh-agent -a $SSH_AUTH_SOCK >/tmp/.ssh-script
    . /tmp/.ssh-script
    echo $SSH_AGENT_PID >/tmp/.ssh-agent-pid
fi


function kill-agent {
    pid=`cat /tmp/.ssh-agent-pid`
    kill $pid
}