set nocompatible

filetype indent plugin on

set hidden

set wildmenu
set wildmode=list:longest
set showcmd
set hlsearch
 
set ignorecase
set smartcase

set history=9999 

set formatoptions=c,q,r,t
set formatoptions+=n 
    
set cpoptions=aABceFsmq

set backspace=indent,eol,start
set autoindent
set nostartofline

set ruler

set laststatus=2
set confirm
 
set visualbell
set t_vb=
 
set mouse=

set cmdheight=2

" set number
set numberwidth=5

set notimeout ttimeout ttimeoutlen=200
set pastetoggle=<F11>

set shiftwidth=4
set softtabstop=4
set expandtab

map Y y$
nnoremap <C-L> :nohl<CR><C-L>
 
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

" set background=dark

syntax on

colorscheme peachpuff 

" hi LineNr       ctermfg=DarkGray
" hi Folded       ctermbg=235 ctermfg=110
" hi Search       ctermfg=202 ctermbg=88
" hi htmlItalic   term=NONE cterm=NONE ctermbg=darkgray ctermfg=gray
" hi NonText      guifg=#4a4a4a ctermfg=DarkGray
" hi SpecialKey   guifg=#4a4a4a ctermfg=DarkGray
" hi StatusLine       ctermbg=235 ctermfg=Gray   cterm=NONE
" hi StatusLineNC     ctermbg=Black      ctermfg=Gray   cterm=NONE   guibg=#222222  guifg=#555555
" hi SpellBad    cterm=NONE ctermbg=132 ctermfg=white
" hi SpellLocal  cterm=NONE ctermbg=102 ctermfg=white
" hi SpellCap    cterm=NONE ctermbg=103 ctermfg=white
" hi CursorLine    cterm=NONE ctermbg=234
" hi CursorLineNr  ctermfg=102 ctermbg=235
" hi ColorColumn   ctermbg=235
" hi Visual       ctermbg=24
" hi MatchParen  ctermfg=190 cterm=none ctermbg=none
