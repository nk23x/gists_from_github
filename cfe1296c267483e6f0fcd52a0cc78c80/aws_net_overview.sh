#/bin/bash

OUTFILE="data/aws_network_$(date +'%Y%m%d%H%M%S%s')"
OUTFILENR="1"

mkdir data

for task in \
"describe-vpn-connections --vpn-connection-ids vpn-XXXXXXXX" \
"describe-customer-gateways --customer-gateway-ids" \
"describe-vpn-gateways" \
"describe-vpcs --vpc-ids" \
describe-vpc-endpoints \
describe-vpc-endpoint-services \
describe-vpc-peering-connections \
describe-internet-gateways \
describe-nat-gateways \
describe-network-acls \
"describe-network-acls --filters Name=vpc-id,Values=vpc-XXXXXXXX" \
describe-security-groups \
"describe-security-groups --filters Name=vpc-id,Values=vpc-XXXXXXXX" \
describe-route-tables \
describe-subnets \
describe-addresses \
describe-hosts \
describe-instances \
describe-tags \
"describe-tags --filters Name=resource-type,Values=customer-gateway" \
"describe-tags --filters Name=resource-type,Values=instance" \
"describe-tags --filters Name=resource-type,Values=internet-gateway" \
"describe-tags --filters Name=resource-type,Values=network-acl" \
"describe-tags --filters Name=resource-type,Values=network-interface" \
"describe-tags --filters Name=resource-type,Values=route-table" \
"describe-tags --filters Name=resource-type,Values=security-group" \
"describe-tags --filters Name=resource-type,Values=subnet" \
"describe-tags --filters Name=resource-type,Values=vpc" \
"describe-tags --filters Name=resource-type,Values=vpn-connection" \
"describe-tags --filters Name=resource-type,Values=vpn-gateway" \
describe-network-interfaces; do 
    if [[ $task = *--* ]]; then 
		TASKNAME="$(echo "$task" | sed 's/describe-//' | sed 's/ --/-/g' | sed 's/ /-/g' | sed 's/Name=//' | sed 's/,Values=/-/')"
	else
		TASKNAME="$(echo "$task" | cut -d ' ' -f 1 | sed 's/describe-//')"
	fi
	aws ec2 $task >> $OUTFILE.$OUTFILENR.$TASKNAME.json
	if [ -e "$(which json_xs)" ]; then
		cat $OUTFILE.$OUTFILENR.$TASKNAME.json | json_xs -f json -t yaml > $OUTFILE.$OUTFILENR.$TASKNAME.yaml
	fi
	OUTFILENR="$(expr $OUTFILENR + 1)"	
done 

echo -e "\n$0 generated these JSON files:\n"
ls -1tr $OUTFILE*.json
echo -e "\n$0 generated these YAML files:\n"
ls -1tr $OUTFILE*.yaml
echo -e "\n\n"
