== ~/.bash_login ==

  if [ -f /usr/bin/aws_bash_completer ]; then
     source '/usr/bin/aws_bash_completer'
   fi  
 
   function awscli { aws $@ --output json  | json_xs -t yaml - ; }

== commands ==

   aws cloudwatch describe-alarms --output json  | json_xs -t yaml - | grep -E 'Arn|State' | sed -r 's/AlarmArn/\nAlarmArn/g;'