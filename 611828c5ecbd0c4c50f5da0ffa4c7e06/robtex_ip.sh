#!/bin/bash
LINKS_BIN="$(which links)"
LOOKUP_IP="$1"
if [[ ! '$LINKS_BIN' ]]; then 
  echo "links browser not found."
elif [[ ! '$LOOKUP_IP' ]]; then 
  echo "nothing to lookup ... try $0 [IP.AD.DR.ESS]"
fi

${LINKS_BIN} -dump https://www.robtex.com/ip-lookup/$1 | \
sed -e '/^RECORDS/,/^THREATMINER/!d;s/\[ Submit \]//g;s/RECORDS//;s/THREATMINER//;'

