#!/bin/bash
show_usage()
{
	echo "Usage: rss-diff -o original -f feed -t trim"
exit
}
MINARGS=2
case $1 in
	"-h"|"--help") show_usage; return 1; ;;
esac

ARGC=$#
if [[ $ARGC -lt $MINARGS ]] ; then
 echo "Too few arguments given (only command is optional)"
 show_usage
return 1;
fi
while getopts "o:f:t:" optname
    do
      case "$optname" in
        "o")
			 original=("$OPTARG")    
          ;;
		"f")
			 feed=("$OPTARG")          
	      ;;
        "t")
			 trim=($OPTARG)          
          ;;
        "?")
          echo "Unknown option $OPTARG"
          ;;
        ":")
          echo "No argument value for option $OPTARG"
          ;;
        *)
        # Should not occur
          echo "Unknown error while processing options"
			show_usage;return 1;
          ;;
      esac
    done
	if [ "$original" ]; then echo ""; else echo "original file is required";show_usage;return 1; fi
	if [ "$feed" ]; then echo ""; else echo "feed is required";show_usage;return 1; fi
	curl $feed > /tmp/feed.txt
	if [ "$trim" ];then contents=$(cat /tmp/feed.txt | tail -n +$trim);else contents=$(cat /tmp/feed.txt);fi
if diff $original <(echo "$contents") >/dev/null ; then
#On a Mac with terminal-notifier for Notification Center Support (https://github.com/alloy/terminal-notifier)
#/usr/bin/terminal-notifier.app/Contents/MacOS/terminal-notifier -title "Feed Is Same" -message "$feed"
echo "Feed Is Same"
rm /tmp/feed.txt
exit
else
#On a Mac with terminal-notifier for Notification Center Support (https://github.com/alloy/terminal-notifier)
#/usr/bin/terminal-notifier.app/Contents/MacOS/terminal-notifier -title "Feed Is Different!" -message "$feed" -open $feed
echo "Feed Is Different!"
if [ "$trim" ];then contents=$(cat /tmp/feed.txt | tail -n +$trim > $original);else contents=$(cat /tmp/feed.txt > $original);fi
rm /tmp/feed.txt
exit
fi