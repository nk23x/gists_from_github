#!/bin/bash
cd $1 || cd /tmp
lftp -c "pget -n 10 -c http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux_x64/$(wget -q -O - http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux_x64/LAST_CHANGE)/chrome-linux.zip"
echo -e "$(du -h $(pwd)/chrome-linux.zip)"
