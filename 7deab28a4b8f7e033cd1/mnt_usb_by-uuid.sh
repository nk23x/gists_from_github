#!/bin/bash

echo >> /etc/fstab 

for d in $@; do 
     mkdir /mnt/usb/$(blkid ${d}|perl -pe 's/(.*)UUID=\"(.*)\" TYPE=\"(.*)\"/$2/g;')
     echo "$(blkid ${d}|perl -pe 's/(.*)UUID=\"(.*)\" TYPE=\"(.*)\"/UUID=$2 \/mnt\/usb\/$2 $3 defaults,rw 1 1/g;')" >> /etc/fstab
done 

mount -av


