# free github related services

Project Management for GitHub: 
   * https://waffle.io/pricing/
   * https://zube.io/#pricing

Deployment:
   * https://www.deployhq.com/packages

Documentation:
   * https://www.gitbook.com/pricing
