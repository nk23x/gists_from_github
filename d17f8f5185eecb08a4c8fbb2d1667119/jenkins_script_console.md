# Jenkins Script Console Tutorial

Jenkins script console allows one to run groovy scripts/code to automate jenkins setup e.g installing plugins, setting configuration parameters. These groovy scripts can be run either through the web ui, or event loaded as scripts using curl from command line. With these groovy scripts, jenkins configurations can be completely automated. 

Lets explore this script console  and learn how to write/load simple scripts. 

## Exploring script console 

From the jenkins UI, choose **Jenkins => Manage Jenkins => Script Console** . You should see script console installed, in which run the following, 

```
println(Jenkins.instance.pluginManager.plugins)

Jenkins.instance

Jenkins.instance.metaClass.methods*.name

```


## Installing Plugins 

Lets pick a script which installs a git plugin, copy it to script console and run. 


```
import jenkins.model.*

def pluginParameter="git slack"
def plugins = pluginParameter.split()
println(plugins)
def instance = Jenkins.getInstance()
def pm = instance.getPluginManager()
def uc = instance.getUpdateCenter()
def installed = false

plugins.each {
  if (!pm.getPlugin(it)) {
    def plugin = uc.getPlugin(it)
    if (plugin) {
      println("Installing " + it)
      plugin.deploy()
      installed = true
    }
  }
}

instance.save()
if (installed)
instance.doSafeRestart()
```
source : https://stackoverflow.com/questions/31457623/jenkins-plugin-installation


#### Exercise 

  * Observe what happens after running the above groovy script ?  Did the plugin get installed?  Check by running the println command that we run earlier to list the plugins. 
  * Install the following plugins
    * slack
    * jira


## Running Groovy Scripts from command line console

Lets write a groovy script which will update the  admin email address for jenkins. You could view the current configs at **Manage Jenkins => Configure Systems => System Admin e-mail address** 

File: update_admin_email.groovy
```
import jenkins.model.*
def jenkinsLocationConfiguration = JenkinsLocationConfiguration.get()
jenkinsLocationConfiguration.setAdminAddress("[Super User] <super@user.org>")
jenkinsLocationConfiguration.save()
```

Before we send this script to jenkins, find out the jenkins uri. e.g. http://139.59.0.187:8081/ 
We also need to get a CSRF token, which is then used as part of  subsequent payload. This is part of additional security measures enabled by default as part of Jenkins 2.0. 


```console
mytoken=$(curl --user 'admin:admin' -s http://<jenkins_uri>/crumbIssuer/api/json | python -c 'import sys,json;j=json.load(sys.stdin);print j["crumbRequestField"] + "=" + j["crumb"]')
```

where replace <jenkins_uri> with actual. 

e.g.
```
mytoken=$(curl --user 'admin:admin' -s http://139.59.0.187:8081/crumbIssuer/api/json | python -c 'import sys,json;j=json.load(sys.stdin);print j["crumbRequestField"] + "=" + j["crumb"]')
```

And load the groovy script 
```
curl --user 'admin:admin'   -d "$mytoken"   --data-urlencode "script=$(<./<script_name>)" http://<jenkins_url>/scriptTex
```

e.g. 
```
curl --user 'admin:admin'   -d "$mytoken"   --data-urlencode "script=$(<./update_admin_email.groovy)" http://139.59.0.187:8081/scriptTex
```

Validate if the email is changed   by visting **Manage Jenkins => Configure Systems => System Admin e-mail address**




#### Reading List 

  * Script Console Demo: https://www.youtube.com/watch?v=T1x2kCGRY1w
  * Script Console Doc:  https://wiki.jenkins.io/display/JENKINS/Jenkins+Script+Console
  * Jobs DSL PLugin 

