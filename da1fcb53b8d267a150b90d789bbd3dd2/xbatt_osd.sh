#!/bin/sh

SLEEP="$1" || SLEEP="10"

if [ -p $(cat /tmp/$(basename $0).pid) ]; then
    exit 1
else
    if [ -f /tmp/$(basename $0).pid ]; then
        rm /tmp/$(basename $0).pid
    fi
    echo "$$" > /tmp/$(basename $0).pid
fi

while true; do
    if [ "$(grep -c off-line /proc/acpi/ac_adapter/AC/state)" == 1 ]; then
        echo "$(acpitool -b)" | sed -r 's/.*Bat.*: / /' | osd_cat - -w -l 2 -a 15 -i 6 -o 20 -p top -c lightgreen -A right -O 1 -u black -f -misc-fixed-medium-r-*-*-12-*-*-*-*-*-* -s 0
    fi
    if [ "$SLEEP" ]; then 
        sleep ${SLEEP}m
    fi
done
