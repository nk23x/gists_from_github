## get token with the scope "delete_repo"
curl -v -u [USER] -X POST https://api.github.com/authorizations -d '{"scopes":["delete_repo"], "note":"token w. delete repo scope"}'

## delete repos using the "delete_repo" token
curl -v -X DELETE -H 'Authorization: token [DELTE_OK_TOKEN]' https://api.github.com/repos/[USER]/[REPO]
## returns "HTTP/1.1 204 No Content" when done