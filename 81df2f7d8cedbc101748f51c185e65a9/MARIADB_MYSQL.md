# Correct mysqldump backup

## for my.cnf
```
BACKUP_TIMESTAMP=`date '+%Y-%m-%d_%H-%M-%S'`
BACKUP_DIR='/mybackupdir'
cp /etc/my.cnf $BACKUP_DIR/my_$BACKUP_TIMESTAMP.cnf
```
## for MySQL databases with MyISAM tables
```
BACKUP_TIMESTAMP=`date '+%Y-%m-%d_%H-%M-%S'`
BACKUP_DIR='/mybackupdir'
mysqldump --user=root --all-databases --lock-all-tables --master-data=1 --flush-privileges \
--flush-logs --triggers --routines --events --hex-blob > $BACKUP_DIR/full_dump_$BACKUP_TIMESTAMP.sql
```
## for MySQL databases with InnoDB tables only
```
BACKUP_TIMESTAMP=`date '+%Y-%m-%d_%H-%M-%S'`
BACKUP_DIR='/mybackupdir'
mysqldump --user=root --all-databases --single-transaction --master-data=1 --flush-privileges \
--flush-logs --triggers --routines --events --hex-blob > $BACKUP_DIR/full_dump_$BACKUP_TIMESTAMP.sql
```
- --flush-privileges works since 5.1.12
- --flush-logs has a bug (#61854) in MySQL 5.5 which fixed in 5.5.21.
- --triggers seems to be enabled by default now. It's unclear to me since when.

While a --single-transaction dump is in process, to ensure a valid dump file (correct table contents 
and binary log coordinates), no other connection should use the following statements: ALTER TABLE, 
CREATE TABLE, DROP TABLE, RENAME TABLE, TRUNCATE TABLE. A consistent read is not isolated from those 
statements, so use of them on a table to be dumped can cause the SELECT that is performed by mysqldump 
to retrieve the table contents to obtain incorrect contents or fail.

With mysqldump from Percona you should also use --lock-for-backup

SOURCE: http://www.fromdual.com/node/1309

# fromdual.com articles
- http://www.fromdual.com/limitations-of-galera-cluster
- http://www.fromdual.com/is-your-mysql-software-cluster-ready 
- mirror: https://archive.is/QeQ0s
- https://archive.is/Kp5Oh
- http://www.fromdual.com/mysql-replication-with-filtering-is-dangerous
- mirror: https://archive.is/9KG9h
