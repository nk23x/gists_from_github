#! /bin/bash
apt-get install -y python-pip
pip install shadowsocks

cat <<EOF > /etc/shadowsocks.json
{
    "server":"0.0.0.0",
    "server_port":53,
    "local_port":1080,
    "password":"secret",
    "timeout":300,
    "method":"aes-256-cfb",
    "fast_open": true,
    "workers": 4
}
EOF

cat <<EOF > /etc/init/shadowsocks.conf
start on filesystem
stop on runlevel [016]

exec /usr/local/bin/ssserver -qq -c /etc/shadowsocks.json --fast-open
EOF

cat <<EOF > /etc/sysctl.d/11-tcp-perf.conf
net.ipv4.tcp_sack = 1
net.ipv4.tcp_window_scaling = 1
fs.file-max = 51200
# max open files
fs.file-max = 51200
# default read buffer
net.core.rmem_default = 65536
# default write buffer
net.core.wmem_default = 65536

net.core.rmem_max = 67108864
net.core.wmem_max = 67108864
net.core.netdev_max_backlog = 250000
net.core.somaxconn = 4096

net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_time = 1200
net.ipv4.ip_local_port_range = 10000 65000
net.ipv4.tcp_max_syn_backlog = 8192
net.ipv4.tcp_max_tw_buckets = 5000
net.ipv4.tcp_fastopen = 3
net.ipv4.tcp_mem = 25600 51200 102400
net.ipv4.tcp_rmem = 4096 87380 67108864
net.ipv4.tcp_wmem = 4096 65536 67108864
net.ipv4.tcp_mtu_probing = 1
net.ipv4.tcp_congestion_control=cubic
EOF

sysctl --load=/etc/sysctl.d/11-tcp-perf.conf

service shadowsocks restart