#!/bin/bash

machine_type=f1-micro
image=ubuntu-14-04


gcloud compute instances create shadowsocks-1 \
    --can-ip-forward --image $image --restart-on-failure \
    --zone asia-east1-b --machine-type $machine_type \
    --metadata-from-file startup-script=bin/shadowsocks-startup.sh
