#!/bin/bash
CR_DIR="$HOME/apps/chrome-linux"
CR_BIN="chrome"
PF_DIR="/usr/lib64/chromium/PepperFlash"
PF_LIB="libpepflashplayer.so"
PF_VER="$(grep '"version":' $PF_DIR/manifest.json | cut -d\" -f4)"

$CR_DIR/$CR_BIN --ppapi-flash-path=$PF_DIR/$PF_LIB --ppapi-flash-version=$PF_VER 2>/dev/null &
