#!/bin/bash
cd /tmp/
lftp -e "pget -n 10 -c http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux_x64/$(wget -q -O - http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux_x64/LAST_CHANGE)/chrome-linux.zip, exit"
echo -e "$(du -h $(pwd)/chrome-linux.zip)"
