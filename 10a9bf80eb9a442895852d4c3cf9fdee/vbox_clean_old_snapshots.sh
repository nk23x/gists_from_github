#!/bin/bash
VM_NAME="$1" 

if [[ -z $1 ]]; then 
  echo "no vm name provided"
  exit 1
fi

for VM_SNP in $(vboxmanage snapshot $VM_NAME list | perl -pe 's/^.*Name:/Name:/g;' | grep -v ' $(date +'%Y%M')' | perl -pe 's/.*UUID: (.*)\)/$1/g;'); do 
  vboxmanage snapshot $VM_NAME delete $SNP
done

