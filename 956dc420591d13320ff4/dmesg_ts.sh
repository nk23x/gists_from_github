#!/bin/sh

## for systems (like sles 11) wher dmesg does not support parameters -T or --ctime
## source: https://www.verboom.net/blog/index.html?single=20110927.0

dmesg | gawk -v uptime=$( grep btime /proc/stat | cut -d ' ' -f 2 ) '/^[[ 0-9.]*]/ { print strftime("[%Y/%m/%d %H:%M:%S]", substr($0,2,index($0,".")-2)+uptime) substr($0,index($0,"]")+1) }' 