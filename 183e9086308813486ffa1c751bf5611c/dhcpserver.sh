#!/usr/bin/env bash


# sudo call integrity check: only root should be able to change script
l=($(ls -l `readlink -f $0`))
[ ${l[0]:2:1} != "-" ] && [ "${l[2]}" != "root" ] ||
[ ${l[0]:5:1} != "-" ] && [ "${l[3]}" != "root" ] ||
[ ${l[0]:8:1} != "-" ] && { echo -e "only root should be able to modify\n${l[@]}"; exit 1;}


# Defaults
IP="192.168.2.1"
PORTS="53 443"

MASK="255.255.255.0"
MASKBITS="/24"

if [ $# -lt 1 ]; then
cat <<USAGE
usage: $0 <eth> [ip] [port ...] [<nat>]

    eth     interface
    ip      IP for interface in '$MASKBITS' network (default '$IP')
    port    List of ports to open on firewall ('$PORTS')
    nat     if set to 1 setup firewall to NAT clients
USAGE
exit
fi

# SET INTERFACE
ETH=$1; shift

# SET IP
shopt -s extglob
if [ "${1/+([0-9]).+([0-9]).+([0-9]).+([0-9])/isip}" == "isip" ]; then
    IP=$1; shift
fi
NETPREFIX=${IP/%.+([0-9])/} # all but last ip octect

# SET NAT CLIENTS OR NOT
USENAT=0
if [ $# -gt 0 ] && [ "${@:$#}" == "1" ]; then
    USENAT=1
    # $@ = all but last arg
    set -- ${@:1:$#-1}
elif [ $# -gt 0 ] && [ "${@:$#}" == "0" ]; then
    set -- ${@:1:$#-1}
fi

# SET FIREWALL PORTS
if [ $# -gt 0 ]; then
    PORTS="$@"
fi
echo "eth: '$ETH'  ip: '$IP'  net: '${NETPREFIX}.0$MASKBITS'  nat: '$USENAT'"
echo "ports: $PORTS"
echo ""
sleep 1

# GO


# kill dhclient on $ETH
PIDS=$(ps -aux | grep dhclient | grep $ETH | awk "{print \$2}" | xargs)
test "$PIDS" && sudo kill $PIDS

set -x

sudo ufw allow in on $ETH from any port 68 to any port 67 proto udp
# could replace. ufw allows multi ports as "n,n,n"
for PORT in $PORTS; do
    sudo ufw allow in on $ETH to any port $PORT
done

sudo ifconfig $ETH $IP netmask 255.255.255.0

if [ $USENAT -ne 0 ]; then
    sudo sysctl -w net.ipv4.ip_forward=1
    sudo iptables -A FORWARD -i $ETH  -s ${NETPREFIX}.0$MASKBITS -m conntrack --ctstate NEW -j ACCEPT
    sudo iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    sudo iptables -t nat -A POSTROUTING  -j MASQUERADE
fi

test -d /tmp/$ETH || mkdir /tmp/$ETH
DIR=/tmp/$ETH

cat >$DIR/dnsmasq.conf <<EOF
dhcp-option=option:router,${IP}
dhcp-range=${NETPREFIX}.10,${NETPREFIX}.254,${MASK},96h
EOF
sudo dnsmasq -d -i $ETH \
  --conf-file="$DIR/dnsmasq.conf" \
  --dhcp-leasefile="$DIR/leases" \
  --pid-file="$DIR/pid"
#  --dhcp-host="00:80:2f:ff:ff:ff,192.168.1.111,HOSTNAME" \
#  --dhcp-option="option:ntp-server,${IP}.1" \
#  --domain=local


# END CLEANUP

sudo ufw delete allow in on $ETH from any port 68 to any port 67 proto udp
for PORT in $PORTS; do
    sudo ufw delete allow in on $ETH to any port $PORT
done
if [ $USENAT -ne 0 ]; then
    sudo sysctl -w net.ipv4.ip_forward=0
    sudo iptables -D FORWARD -i $ETH  -s ${NETPREFIX}.0$MASKBITS -m conntrack --ctstate NEW -j ACCEPT
    sudo iptables -D FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    sudo iptables -t nat -F
fi
