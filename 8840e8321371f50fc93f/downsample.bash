#!/bin/bash 
[[ ! -n "$1" ]] && {
  echo "Usage: ./downsample <input.wav> <output.wav> "
  echo "       DOWNSAMPLE=6 MONO=1 ./downsample in.wav out.wav ";
  echo "       TREBLE=3 RATE=22500 DOWNSAMPLE=4 GAIN=1 ./downsample in.wav out.wav ";
  exit
}

calc(){
  echo "$1" | bc -l
}

gate(){
  # a-law has a compader
  sox -V "$1" -e signed -b 8 -r 22500 -c 1 "$1".tmp.wav
  mv "$1".tmp.wav "$1" 
  sox -V "$1" -e a-law -b 8 -r 22500 -c 1 "$1".tmp.wav
  mv "$1".tmp.wav "$1" 
}

downsample(){
  [[ -f "$2" ]] && echo "deleting!!!!!!!!!" && rm "$2"
  [[ -n $RATE ]] && rate=$RATE || rate=30000
  [[ -n $DOWNSAMPLE ]] && downsample="norm downsample $DOWNSAMPLE upsample $DOWNSAMPLE" || downsample=""
  
  sox "$1" -n stats 2>&1 | grep Left &>/dev/null
  [[ $? == 0 ]] && channels=2 || channels=1
  [[ -n $MONO ]] && channels=1
  [[ ! -n $BITS ]] && BITS=16 
  [[ $BITS == 8 ]] && ENCODING=a-law || ENCODING=signed-integer
  soxopts="-e $ENCODING -b $BITS -D -r $rate -c $channels"

  cp "$1" /tmp/in.wav

  [[ -n $TREBLE ]] && {
    sox /tmp/in.wav -c 2 "$2".hp.wav highpass $(( (rate/2)-2000)) gain +$TREBLE
    sox -M /tmp/in.wav "$2".hp.wav -c 2 "$2".final.wav
    mv "$2".final.wav /tmp/in.wav ;
  }

  echo sox -V /tmp/in.wav ${soxopts} "$2" ${downsample}
  sox -V /tmp/in.wav ${soxopts} "$2" ${downsample}
  [[ -n GAIN ]] && {
    sox "$2" "$2".gain.wav gain $GAIN
    mv "$2".gain.wav "$2";
  }
}

downsample "$@"
