#!/bin/sh

case "${1}" in

  'getsize')
    BZ2FILE="$2"
    bzip2 --decompress --stdout ${BZ2FILE} | wc --bytes
  ;;
  
  'relrzip')
    BZ2FILE="$2"
    bzip2 --decompress --stdout ${BZ2FILE} | lrzip -vv -l -o $(basename --suffix=.bz2 ${BZ2FILE}).lrzip
  ;;

  '*')
    cat <<-EOF
  
   SYNTAX: 
   $0 [getsize | relrzip] [file.bz2]
  
   PARAMETERS:
   getsize = get the size of the decompressed file w/o decompressing it
   relrzip = re-compress bzip2 file using lrzip 

EOF
   ;;

esac
