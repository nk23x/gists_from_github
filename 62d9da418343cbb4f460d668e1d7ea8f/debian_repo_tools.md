Create your own Repository:

* https://github.com/geek1011/repogen

* https://tracker.debian.org/pkg/reprepro
  * https://wiki.debian.org/DebianRepository/SetupWithReprepro
  * https://wiki.debian.org/PartialBackportMirrorWithPackageApproval
  * https://blog.packagecloud.io/eng/2017/03/23/create-debian-repository-reprepro/

* https://github.com/go-debos/debos
  * https://github.com/ana/debos-example
  * https://www.collabora.com/news-and-blog/blog/2018/06/27/introducing-debos/


* https://www.aptly.info/
  * https://github.com/aptly-dev/aptly

Services:

* https://packagecloud.io/l/apt-repository

Other: Debian Sources List Generators

* https://debgen.simplylinux.ch/index.php?generate
* https://mirrors.ustc.edu.cn/repogen/
* https://deb.geek1011.net/packages/stable/repogen/

