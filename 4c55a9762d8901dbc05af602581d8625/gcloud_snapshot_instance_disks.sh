#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DATE_TIME="$(/bin/date "+%Y%m%d%H%M%S")"
CURL_BIN="/usr/bin/curl --silent --header Metadata-Flavor:Google"
INSTANCE_NAME="$(${CURL_BIN} "http://metadata.google.internal/computeMetadata/v1/instance/name")"
INSTANCE_ZONE="$(${CURL_BIN} "http://metadata.google.internal/computeMetadata/v1/instance/zone")"

for INSTANCE_DISK in $(${CURL_BIN} "http://metadata.google.internal/computeMetadata/v1/instance/disks/?recursive=true&alt=text" | grep 'device-name' | cut -d' ' -f 2); do
  gcloud compute disks snapshot "${INSTANCE_DISK}" --snapshot-names "${INSTANCE_DISK}-${DATE_TIME}" --zone "${INSTANCE_ZONE}"
  gcloud compute snapshots delete --quiet $(gcloud compute snapshots list --format="table(name)" --filter="name ~ ${INSTANCE_DISK}" --sort-by=createTime | grep -v NAME | head -n 1)
done
