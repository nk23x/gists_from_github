Useful Commands
===============
Some of it related to embedded analysis, some of it not.

Table of Contents
-----------------

  * [Dump or copy via nc](#dump-or-copy-via-nc)
  * [Dump or copy via ssh](#dump-or-copy-via-ssh)
  * [Copy proc or sys](#copy-proc-or-sys)
  * [TFTP walk](#tftp-walk)
  * [tcpdump filters](#tcpdump-filters)
  * [Check SSL Certificates](#check-ssl-certificates)
  * [OSX DHCPD/TFTPD](#osx-dhcpdtftpd)
  * [Linux DHCP Server](#linux-dhcp-server)
  * [OSX DNS](#osx-dns)
  * [Remote Wireshark](#remote-wireshark)
  * [Embedded Tcpdump](#embedded-tcpdump)
  * [Unix Socket debug](#unix-socket-debug)
  * [Gigbit Sniffing](#gigbit-sniffing)
  * [MiTM SSL](#mitm-ssl)
  * [Proxy all packets per program](#proxy-all-packets-per-program)
  * [Quick split of file into parts](#quick-split-of-file-into-parts)
  * [Connecting to onioned ssh](#connecting-to-onioned-ssh)
  * [Binary edit on shell](#binary-edit-on-shell)
  * [Dynamic Assisted RE](#dynamic-assisted-re)

Dump or copy via nc
===================
On destination: ``nc -l 4444 > incoming.tar.bz2`` 
On source:      ``tar -jcv files dirs files | nc <client> 4444``

Alternatives

On destination: ``nc -l 4444 | tar -jxvk``  
On source:      ``tar -jcv /[^p]* | nc <client> 4444`` (avoid proc)  
or source:      ``tar -zxcv / --one-file-system / | nc <client> 4444`` (full gnu tar, restrict to disk)  
or source:      ``dd if=/dev/mtdblock0 bs=2048 | nc <client> 4444`` (block device)

If destination cannot listen you can also reverse the order with:
   
On destination: ``nc <source_ip> 4444 > incoming``  
On source:      ``cat outgoing | nc -l 4444``  
  
Check rules on destination firewall (iptables/ufw linux/ubuntu, 'ipfw add allow 4444 from any to any' osx) and source (``iptables -A OUTPUT -j ACCEPT``)


Dump or copy via ssh
====================
From destination:  ``ssh root@source "tar -czpf - /" > sshfs.tar.gz``  
From source:       ``tar -cpf - / | ssh user@destination "tar xpf - -C dest_dir"``  
Or from source:    ``cd /; tar -czpf - $(ls /|grep -v proc|grep -v sys) | ssh -o PubkeyAuthentication=no -o PreferredAuthentications=password user@dest "cat > fs.tar.gz"``

If you need to open your firewall (``iptables -A OUTPUT -j ACCEPT`` same on INPUT if you really do not care about your target). Check with a packet sniffer.

For source to destination method, add temporary user:

```bash
dir=$(mktemp --dry-run)
date=$(date -d tomorrow +%Y-%m-%d)
echo "user added will expire in on \"$date\" with home at \"$dir\""
sudo userdel tmpuser
sudo useradd --expiredate $date --no-user-group \
	-c "Tmp user" --create-home --home "$dir" \
	--shell /bin/bash tmpuser
sudo passwd tmpuser
```


Copy proc or sys
================
```bash
SRC=/proc
DST=/var/tmp
MAXDEPTH="-maxdepth 4" #blank for no max
mkdir -p $DST/$SRC
find $SRC $MAXDEPTH | while read F ; do
   echo $F | grep "proc/kmsg"     && continue #blocking
   echo $F
   test -L $F && cp -a $F $DST/$F && continue #symlink
   test -d $F && mkdir $DST/$F    && continue #directory
   test -f $F && cat $F > $DST/$F && continue #file
done
```


TFTP walk
=========
Given a file list

```bash
for file in file1 file2 file3
do
	echo get $file | tftp <TARGET_IP>
done 
```


tcpdump filters
===============
``tcpdump -i <inteface> not port 22``  
``tcpdump -n -w output.cap -i <inteface> not port 22`` (-n ignore hostname, add -s 0 to save full packet)  



Check SSL Certificates
======================
Using cut+paste of cert: ``openssl x509  -text -noout``   
Using file: ``openssl x509  -text -noout -in /tmp/file``  
Checking priv key: ``openssl rsa -text -noout -in /tmp/privkey.pem``  
For public keys add: ``-pubin``

Comparing to values of the modulos to see that they correspond to eachother.

``-noout``: write result to stdout  
``-text``: output information about in plaintext form.

Get rsa pubkey from priv: ``openssl rsa -text -pubout -in /tmp/privkey``



OSX DHCPD/TFTPD
===============
```
# Add host:
sudo gvim /etc/bootptab
sudo /bin/launchctl load -w /System/Library/LaunchDaemons/bootps.plist
sudo /bin/launchctl unload -w /System/Library/LaunchDaemons/bootps.plist
sudo /bin/launchctl load -w /System/Library/LaunchDaemons/tftp.plist
sudo /bin/launchctl unload -w /System/Library/LaunchDaemons/tftp.plist
```

Optionbally bootpd will start when enabling internet sharing.

Linux DHCP Server
=================

```
alias s=sudo
s killall dhclient
s ifconfig eth0 192.168.1.1 netmask 255.255.255.0
d=`mktemp -d`
cat >$d/dnsmasq.conf << EOF
 dhcp-option=option:router,192.168.1.1
 dhcp-range=192.168.1.10,192.168.1.254,255.255.255.0,96h
EOF
dnsmasq -d -i eth0 \
 --conf-file=$d/dnsmasq.conf \
 --leases-file=$d/leases \
 --pid-file=$d/pid

s sysctl .net.ipv4.ip_forward
s sysctl .net.ipv4.ip_forward=1
# NAT
s iptables -A FORWARD -i wlan0 -o eth0 -s 192.168.1.0/24 -m conntrack --ctstate NEW -j ACCEPT
s iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
s iptables -A POSTROUTING -t nat -j MASQUERADE
# OR
#s iptables -A POSTROUTING -t nat -o eth0 -j MASQUERADE
```


OSX DNS
=======
The DNS proxy used with internet sharing for pre Mavericks [used named](http://soundofsyntax.wordpress.com/2012/02/09/osx-internet-sharing-under-the-hood/) zones. For > Mavericks mDNSResponder is used ([source code](http://www.opensource.apple.com/source/mDNSResponder/mDNSResponder-320.10/)). It might be possible to configure the zones somehow but I could not figure it out yet (dig around ``/etc/dnsextd.conf``). Instead I'm using dnsmasq (discussions [1](http://mikeferrier.com/2011/04/04/setting-up-wildcard-dns-on-localhost-domains-on-osx/) [2](http://passingcuriosity.com/2013/dnsmasq-dev-osx/)). In fact you should **use dnsmasq** anyway. Apple appear to change their DNS/Bootp/InternetSharing configurations every release:

```
#execute this interface before executing dhcpd or internet sharing
sudo ifconfig en0 192.168.2.1
sudo dnsmasq --no-daemon -listen-address=192.168.2.1 \
     --address=/localtest./192.168.2.22
```

Test passthrough ``dig @192.168.2.1 google.com`` and localtest ``dig @192.168.2.1 localtest``. 

To configure InternetSharing or bootpd to provide clients with your IP edit ``/etc/bootpd.plist`` and ``/etc/bootptab`` if you want to provide a file.

```
<key>dhcp_domain_name_server</key>
<array>
        <string>192.168.2.1</string>
</array>
```

Remote Wireshark
================
Only works on wireshark installations that support the option to pipe input. Tcpdump output of ``<remote_listener>`` eth0 over SSH to local wireshark:

   ``ssh <remote_listener> tcpdump -s0 -w - 'not port 22' | wireshark -k -i -``
   

Embedded Tcpdump
================
If a target device does not provide TCP dump, or any other bin for that matter, you can attempt to find one *mostly* compatible with the target linux system using a distro's cross platform packages, such as debian.

Download:

Just download copy some tcpdump for the platform. When you execute you should get some errors telling you the version of libc you need, or libpcap. Then walk back through different distro releases until you find a tcpdump with prerequisites that seem to match those required on the platform. 

* [tcpdump](http://archive.debian.net/etch/tcpdump)
* [glibc](http://archive.debian.net/etch/libc6)
* [libpcap0.8](http://archive.debian.net/etch/libpcap0.8)

Extract packages, such as: ``ar -x libpcap0.8_0.9.5-1_mipsel.deb && tar zxvf data.tar.gz``. Then Copy over somehow.

On target:

* ``ln -s libpcap.so.0.8 libpcap.so.0.9.8``


Unix Socket debug
=================
Can use either Netcat-openbsd, socat or strace. These methods, except for strace, allow you to debug and connect ([see](https://superuser.com/questions/484671/can-i-monitor-a-local-unix-domain-socket-like-tcpdump)):

* ``nc -U /path/to/unixsocket`` 
* ``socat UNIX-CONNECT:/path/to/unixsocket STDIN``
* ``strace -e trace=read,write -e read=29,30 -e write=29,30 -p <pid>`` 29,30 are file descriptors (lsof or proc) 
* 

For embedded target example - netcat-openbsd:

```
wget http://ftp.de.debian.org/debian-archive/debian/pool/main/n/netcat-openbsd/netcat-openbsd_1.89-3_mipsel.deb
wget http://ftp.de.debian.org/debian-archive/debian/pool/main/g/glib2.0/libglib2.0-0_2.16.6-3_mipsel.deb
wget http://ftp.de.debian.org/debian-archive/debian/pool/main/p/pcre3/libpcre3_7.6-2.1_mipsel.deb
wget http://ftp.de.debian.org/debian-archive/debian/pool/main/v/vim/vim-common_7.0-122+1etch5_mipsel.deb
for deb in ./*.deb; do ar -x $deb; tar zxvf data.tar.gz; done
tar -cf nc.tar bin/nc.openbsd usr/lib/libglib-2.0.so.0* usr/lib/libpcre.so.3* usr/bin/xxd
cat nc.tar | nc -l 4444
```

target:

```
nc 192.168.2.1 4444 > /var/tmp/nc.tar
cd /var/tmp
tar xvf nc.tar
LD_LIBRARY_PATH=/var/tmp/usr/lib /var/tmp/bin/nc.openbsd -U /path/to/unixsocket | /var/tmp/usr/bin/xxd
```


Or socat:

```
wget http://ftp.de.debian.org/debian-archive/debian/pool/main/s/socat/socat_1.4.3.1-1_mipsel.deb
wget http://ftp.de.debian.org/debian-archive/debian/pool/main/r/readline5/libreadline5_5.2-2_mipsel.deb
wget http://ftp.de.debian.org/debian-archive/debian/pool/main/n/ncurses/libncurses5_5.5-5_mipsel.deb
for deb in ./*.deb; do ar -x $deb; tar zxvf data.tar.gz; done
tar -cf socat.tar usr/bin/socat lib/libreadline* lib/libncurses* 
cat socat.tar | nc -l 4444
```

target (plugin.ctl socket in question):


```
nc 192.168.2.1 4444 > /var/tmp/socat.tar
cd /var/tmp
tar xvf socat.tar
SOCKET=/var/run/plugin.ctl
mv $SOCKET /var/run/sock.original && \
LD_LIBRARY_PATH=/var/tmp/lib /var/tmp/usr/bin/socat -t100 -x -v UNIX-LISTEN:$SOCKET,mode=777,reuseaddr,fork UNIX-CONNECT:/var/run/sock.original && \
mv /var/run/sock.original $SOCKET
```


Gigbit Sniffing
===============
The world of devices are moving to gigabit only interfaces. A simple hub for sniffing might not due. Using a 2nd [gigabit network card](http://www.sitecom.com/en/gigabit-network-usb-adapter/ln-031/p/75) setup a bridge (linux/osx) with:

```
sudo ifconfig bridge0 create
sudo ifconfig bridge0 addm en0 addm en6
sudo ifconfig bridge0 up
```

MiTM SSL
========
[Via burp](http://portswigger.net/burp/), [Via apache](http://hints.macworld.com/article.php?story=20080304022413775), [Via mitmproxy from cortesi](http://mitmproxy.org/), [Via proxystrike](https://code.google.com/p/proxystrike). In firefox might set in ``about:config`` to true: ``browser.xul.error_pages.enabled`` and ``browser.xul.error_pages.expert_bad_cert``.

* Burpsuite: 
    * Import a given cert from target
    * Export burp's self signed cert
        * [import to browser](https://portswigger.net/burp/help/proxy_options_installingCAcert.html)
        * import to java: ``keytool -import -alias burl -file <exportedburpcertfile>``
    * PS: if require upstream proxy fill in under burp Options / Connections / Upstream Proxy Servers. eg.: Dest *, host <proxyIP>, port <port>. Likely do NOT require transparent proxying then in the Proxy / Options

Proxy all packets per program
=============================
Force all packets from programs running with group ID through a proxy on port 8080

	GRP=proxy-all
	iptables -t mangle -A OUTPUT -m owner --gid-owner $GRP -j MARK --set-mark 2
	iptables -t mangle -A OUTPUT -m owner --gid-owner $GRP -j CONNMARK --save-mark
	iptables -t nat -A OUTPUT -m mark --mark 2 -p tcp --dport 1:65535 -j DNAT --to 127.0.0.1:8088
	iptables -A OUTPUT -m mark --mark 2 -p tcp --dport 1:65535 -j LOG --log-prefix 'PACKETPROX' --log-level 0
	sg -c "firefox"



Quick split of file into parts
==============================
   ``for i in 0x06171cb4 0x06671547; do splitfile.sh <filename> $i 0xffffffff <filename>_$i; done``

  or
    
   ``for i in 0x06171cb4 0x06671547; do dd bs=1 if=<filename> skip=$(($i)) of=<filename>_$i; done``

Connecting to onioned ssh
=========================

Using Tor to remotely connect to a SSH host behind iron-curtains.

   On server:
  
   ``mkdir /var/lib/tor/ssh`` and set in torrc ``HiddenServiceDir /var/lib/tor/ssh/`` and ``HiddenServicePort 22 127.0.0.1:22``. To find ``onionhostname`` ``cat /var/lib/tor/ssh``.

   On client:

   Add to ~/.ssh/config ``Host *.onion`` and ``ProxyCommand socat STDIO SOCKS4A:localhost:%h:%p,socksport=9050``. Optionally add a helpfull alias that uses netcat to route through tor ``alias tor-ssh='ssh -o ProxyCommand="nc -X 4 -x localhost:9050 %h %p"'``.  now ``ssh user@onionhostname.onion``
   

Binary Edit on Shell
====================

* Example with base64 encoded binary:  
  ``echo "cSEk" | base64 -d | xxd -p | sed 's/71/72/' | xxd -p -r | base64``  
  turns "q!$" into "r!$"  
  "cSEk" is base64 for "q!$"  
  xxd turns the binary hex string  
  sed changes the hex (0x71=q, 0x72=r)  
  xxd turns modified hex into binary


Dynamic Assisted RE
===================

Find PID's with known thread or process name (also try ``sched`` in addition to ``comm``):
```bash
find /proc -name comm | while read F; do 
  grep gps $F >/dev/null && echo $F; 
done
```

Find pid's that load a library, show offset:
```bash
find /proc -name maps | while read F; do 
  grep -i driver.so $F && echo $F; 
done
```


Find pid responsible for network port:
```bash
# Monitor on target with:
for pid in `ls`; do
  echo -en "\n\n$pid\n";
  strace -i -v -p $pid 2>&1 \
   | egrep -v 'read|mmap|fstat|open|time|ioctl|write|lseek|pselect|munmap|close|sigreturn|tgkill|nanosleep|rt_sig|restart_|select|pause|futex|SIGUSR1|gettid|madvise|exit|get_robus|clone|child_stack|detached|unfinished' &
  sleep 5 && killall strace;
done

# Stimulate from other host with:
while [ 1 ]; do 
  echo "qwerty" | nc $IP $PORT & p=$!; 
  sleep 1 && kill $p; 
done
```

MITM Layer 2
============

Ettercap: ``s ettercap -T -Q -i wlan0 -o -M arp /<ROUTER>/ /<TARGET>/`` (T text interface, Q superquiet, o onlymitm)

Examples (from man)

    ettercap -T -M arp // //
       Perform the ARP poisoning attack against all the hosts in the LAN.
    ettercap -T -M arp:remote /192.168.1.1/ /192.168.1.2-10/
       Perform  the  ARP poisoning against the gateway and the host in the
       lan between 2 and 10. The 'remote' option is needed to be able to 
       sniff the remote traffic the hosts make through the gateway.
