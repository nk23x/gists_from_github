Two ways to do it, but only worked for me so I'll put it first and the second for reference:

    $ openssl pkcs12 -export -in hostname.crt -inkey hsotname.key -out hostname.p12
    $ openssl pkcs12 -in hostname.p12 -nodes -out hostname.pem

Other options for this method in comments below:

    # Note, the -certfile root.crt appends all CA certs to the export, I've never needed these so it's optional for my personal steps
    $ openssl pkcs12 -export -in hostname.crt -inkey hsotname.key -certfile root.crt -out hostname.p12
    # Note, I've always had my hostname.crt as part of my .pem, so I keep my certs but apparently you may not have to, hence the nocerts flag being an extra option in this sample
    $ openssl pkcs12 -in hostname.p12 -nocerts -nodes -out hostname.pem

The second method hasn't ever worked for me on the servers I've setup, but here it is for reference:

    $ openssl x509 -in hostname.crt -inform DER -out hostname.crt.pem -outform PEM
    $ openssl rsa -in hostname.key -out hostname.key.pem -outform PEM

Then to create the .pem I usually use just concat the two together with the PEM formatted certificate first and the key second.