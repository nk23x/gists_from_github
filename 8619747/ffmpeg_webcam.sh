#!/bin/bash 
while true; do  
  if [[ $(df / -m --output=avail | grep -v Avail) -gt "500" ]]; then
        ffmpeg -an \
        -f video4linux2 \
        -i /dev/video1 \
        -video_size 320x240 \
        -t 00:10:00 \
        -framerate 8 \
        -vf "hflip,vflip,format=yuv420p" \
        -metadata:s:v rotate=0 \
        -qscale:v 6 \
        -n \
        -loglevel quiet \
        /tmp/$(date +%s-%N).ogv
        sleep 2s
    else 
        exit 1
    fi
done
