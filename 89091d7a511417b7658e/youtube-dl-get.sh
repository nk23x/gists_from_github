## get all items from the list
youtube-dl -4 --youtube-skip-dash-manifest -w --no-check-certificate --prefer-insecure --no-part -i -o "%(title)s.%(ext)s" -x --audio-format mp3 --audio-quality 3 -i -c https://www.youtube.com/playlist?list=PLWXLb9fC8Kbir7dGd5ErwUffaPIvwyA4J

## or with playlist start position:
youtube-dl -4 --youtube-skip-dash-manifest -w --no-check-certificate --prefer-insecure --no-part --playlist-start 67 -i -o "%(title)s.%(ext)s" -x --audio-format mp3 --audio-quality 3 -i -c https://www.youtube.com/playlist?list=PLWXLb9fC8Kbir7dGd5ErwUffaPIvwyA4J

## get a single track
youtube-dl -4 --youtube-skip-dash-manifest -v -w --no-check-certificate --prefer-insecure --no-part --print-traffic -i -o "%(title)s.%(ext)s" -x --audio-format mp3 --audio-quality 3 -i -c https://www.youtube.com/watch?v=0cO3jKsGzoE

