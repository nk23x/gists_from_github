#!/bin/bash

case "${1}" in 
	'-m') 
		$(which wget) -q -O - -U "Mozilla/4.0" http://www.commandlinefu.com/commands/matching/$2/plaintext 
		;;
    '-t')
		$(which wget) -q -O - -U "Mozilla/4.0" http://www.commandlinefu.com/commands/tagged/$2/plaintext
		;;
	*)
		echo -e "\n\n  usage:\n\n  $(basename $0) [-m|-t ] [command]\n\n  -t = tagged\n  -m = matching\n\n  $(basename $0) -m ls\n  $(basename $0) -t ls\n\n"
		;;
esac   
