#!/bin/sh

BACKUP_THESE_VMS="$@" 

STORES="/vmfs/volumes"

DATASTORES="datastore-1 datastore-2"
BACKUPSTORE="backupstore"

for VM in ${BACKUP_THESE_VMS}; do 

  VM_NAME="$(echo $VM | xargs)"

  VM_ID_AND_STORAGE="$(vim-cmd vmsvc/getallvms | \
    grep -e '^[0-9]' | grep ${VM_NAME}.vmx | \
    sed -r 's/.vmx .*/.vmx/g; s/\[//g; s/\] /\//g; s/[ \t]*/ /g; s/^ //g; s/ /;/g;')"

  VM_ID="$(echo ${VM_ID_AND_STORAGE} | cut -d';' -f1)"
  
  ##
  ## we do not need this, yet but it works:
  ##
  ##  VM_STORAGE_PATH="$STORES/$(echo ${VM_ID_AND_STORAGE} | cut -d';' -f3)"
  ##

  if [ -z $VM_ID ]; then 
  
    echo "${VM_NAME} (ID: $VM_ID) not found on $(hostname)."

    continue
  
  else 

    BACKUPSTORE_VM="${STORES}/${BACKUPSTORE}/${VM_NAME}/$(date +'%Y%m%d')"
    
    mkdir -p ${BACKUPSTORE_VM}
   
    for VMDK_SOURCE in $(vim-cmd vmsvc/get.filelayoutex $VM_ID | grep 'vmdk' | sed -r 's/^.*\[//g;s/\] /\//g;s/\",//g;'); do
      
      VMDK_TARGET="$(basename ${VMDK_SOURCE})"
 
      if [ ! -z $(head -n 1 ${STORES}/${VMDK_SOURCE} | grep -c 'DescriptorFile') ]; then 
      
         if [ ! -z $(echo ${VMDK_SOURCE} | grep '\-flat') ]; then                                  
                                                                                                        
            vmkfstools -i ${STORES}/${VMDK_SOURCE} -N ${BACKUPSTORE_VM}/${VMDK_TARGET}
         
         else 
         
            cp ${STORES}/${VMDK_SOURCE} ${BACKUPSTORE_VM}/${VMDK_TARGET}

         fi
         
      else
         
         vmkfstools -i ${STORES}/${VMDK_SOURCE} -N ${BACKUPSTORE_VM}/${VMDK_TARGET}
      fi
   
    done
  
    VMX_SOURCE="${STORES}/$(vim-cmd vmsvc/get.filelayout $VM_ID | grep 'vmPathName' | sed -r 's/^.* = \"\[//; s/\] /\//; s/\",//;' | xargs)"

    VMX_TARGET="$(basename ${VMX_SOURCE})"
    
    cp ${VMX_SOURCE} ${BACKUPSTORE_VM}/${VMX_TARGET}
    
    if [ -f ${VMX_SOURCE}f ]; then
    
      cp ${VMX_SOURCE}f ${BACKUPSTORE_VM}/${VMX_TARGET}f
      
    fi
    
    NVRAM_SOURCE="$(echo ${VMX_SOURCE} | sed -r 's/.vmx/.nvram/;')"

    NVRAM_TARGET="$(basename ${NVRAM_SOURCE})"
    
    cp ${NVRAM_SOURCE} ${BACKUPSTORE_VM}/${NVRAM_TARGET}
 
  fi

done
