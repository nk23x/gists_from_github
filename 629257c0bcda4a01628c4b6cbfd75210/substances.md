
# substances (2016/12)

* http://rationalwiki.org/wiki/Ephedrine
* http://dr.ivan.tripod.com/id17.html - https://archive.is/EABfa

## ephedra howto quotes

### Dr. Frühling 

Eine chemische Analyse des Extraktionsvorganges können wir Dir nicht erläutern, da wir keine SpezialistInnen auf diesem Gebiet sind. 

Unter http://www.erowid.org/chemicals/ephedrine/ephedrine.shtml kannst Du den Vorgang auf englisch nachlesen.

Ephedrin (Ephedra Herba) ist das Hauptalkaloid der Ephedrapflanze. Es wird auch behauptet, aus der Stammpflanze "Ephedra Sinica" am meisten Ephedrin gewinnen zu können (artverwandte Sträucher sind "Ephedra intermedia" und "Ephedra equisetena").

Eine Möglichkeit, das Ephedrin zu extrahieren, ist das Einlegen des Krautes in reinen Zitronensaft (frisch ausgepresst oder fertiges Konzentrat aus dem Supermarkt). 

Die Mischung sollte mindestens 12 Stunden ziehen. Um das Konzentrat zu gewinnen, muss man es danach durch ein Sieb abgießen. In diesem Fall erreicht man eine Ephedrinkonzentration von ca. 0,5% bis 3,5% (ausgehend von der Pflanze). 

Realistisch sind Werte von 0,7% bis 1,7%. Allerdings schwankt die Qualität der pflanzlichen Droge schon allein durch Umwelteinflüsse oder Erntezeit. Kapseln oder Tabletten sind sicherer zu dosieren und auch geschmacklich neutral.

Außerdem kannst Du versuchen, Dir Ratschläge aus pharmakologischen Einrichtungen, Laboren, Apotheken etc. zu holen (um Risiken wie z.B. Verunreinigungen zu minimieren). 

Frage in diesem Zusammenhang nach der sogenannten "Kalten bzw. Heißen Alkohol/Wasser-Extraktion".

siehe auch: http://psychotropicon.info/ephedraextraktion-am-kuchentisch/

-----

* https://archive.is/JIXrc
* https://archive.is/JIXrc/83189c06b787ddd6864dbedc92caa321b8d4d38f/scr.png

-----

### unser kleines Ephedrarezept abgeleitet aus dem Buch "Orientalische Fröhlichkeitspillen":

Man nehme ca. 100g Ephedra und koche das zusammen mit 2-3 Zimtstangen, ner Prise Zucker und allem weihnachtlichen Gewürzen die man so im Haus hat, in nem Topf mit reichlich Wasser aus. 

Das Wasser wechselt man immer wieder aus (2-3 Stunden bei kleiner Flamme köcheln lassen) und sammelt es in einem zweiten Topf.

Das Spielchen wiederholt man 2-3 mal und preßt dann die Reste durch ein Küchensieb.

Jetzt kocht man das Wasser ein bis man eine braune Pampe hat.

Diese gibt man auf ein Backpapier und schiebt es bei kleiner Hitze in den Backofen.

Man bekommt dann eine ganz feste Platte, diese kann man mahlen und z.B. im Ü-Ei immer mit sich rumschleppen oder ... 

-----
* source: http://www.salvia-community.net/index.php?showtopic=2968&view=findpost&p=31714
* mirror: https://archive.is/HDDpO
* screenshot: https://archive.is/HDDpO/3e248fcef2bca3ac5f3fbf4d86897b3cfd24a748/scr.png
-----
* source: http://www.joergo.de/ephedra-nat/
* mirror: https://archive.is/5bDNM
-----

### Durchführung

100 g grob gepulvertes Ephedrakraut werden zu einer Lösung von 50 g Natriumcarbonat in 150 cm3 Wasser in einem 1000 cm3 Becherglas gegeben, mit einem Glasstab durchmischt und 2 h stehen gelassen.

Man füllt das Perkolationsrohr zu einem Drittel mit Dichlormethan und verschließt die Öffnung vor dem Hahn mit einem Glaswattebausch, der mit einer Seesandschicht von ca. 1 cm Höhe beschwert wird. 

Der Drogenbrei wird portionsweise in die so vorbereitete Perkolationssäule überführt. 

* Die Droge soll sich luftblasenfrei im Perkolationsrohr absetzen. 
* Die Droge wird mit einem Glaswattebausch abgedeckt und leicht zusammengedrückt. 

Man perkoliert durch kontinuierliche Zugabe von 1000 cm3 Dichlormethan. 

Das Perkolat wird portionsweise in einem 500 cm3-Rundkolben am Rotationsverdampfer eingeengt bis zu einem Volumen von 5 cm3.
Davon werden 5 mm3 mit 0,5 cm3 Chloroform verdünnt (= DC-Lösung a).

Der eingeengte Extrakt wird mit 30 cm3 Ether versetzt. 

Die etherische Lösung wird in einem 250 cm3-Scheidetrichter dreimal mit je 50 cm3 0,5 N-Salzsäure ausgeschüttelt. 

Die vereinigten, salzsauren Extrakte werden über einen Trichter mit Faltenfilter, in dem sich 50 g Kaliumcarbonat befinden, in einen 500 cm3-Scheidetrichter filtriert. 

In diesem wird die neutrale bis alkalische Lösung viermal mit je 75 cm3 Dichlormethan ausgeschüttelt. Die vereinigten Dichlormethanauszüge werden über wasserfreiem Natriumsulfat getrocknet und portionsweise in einem 250 cm3-Rundkolben am Rotationsverdampfer eingeengt. 

Das anfallende gelborange-farbene Öl (5 mm3 werden in 1 cm3 Chloroform verdünnt = DC-Lösung b) wird in einer Mischung aus 20 cm3 Ether und 20 cm3 Dichlormethan aufgenommen. 

Auf den 250 cm3-Rundkolben setzt man den passenden Gaswaschflaschenaufsatz. 

Falls keine Chlorwasserstoff-Gasflasche zur Verfügung steht, wird aus einer Gasentwicklungsapparatur, bestehend aus einem 500 cm3-Dreihals-Rundkolben mit passendem Gasableitungsrohr und einem 100 cm3-Tropftrichter durch tropfenweise Zugabe von 20 cm3 konz. Schwefelsäure zu 10 g Kochsalz Chlorwasserstoff entwickelt. 

Über PVC-Schlauchverbindungen durchströmt das Gas zum Trocknen erst eine mit 50 cm3 konz. Schwefelsäure beschickte Waschflasche. 

Nach Passieren einer nachgeschalteten Sicherheitswaschflasche wird der getrocknete Chlorwasserstoff in die etherische Alkaloidbasenlösung eingeleitet. 

Der überschüssige Chlorwasserstoff wird in eine Waschflasche mit lOproz. Natronlauge geleitet. 

Nach etwa 10 min bildet sich in der etherischen Lösung infolge Fällung der Alkaloidhydrochloride ein schmutzigweißer Niederschlag von Ephedrinhydrochlorid neben Hydrochloriden der Begleitalkaloide. 

Daraufhin wird die Chlorwasserstoffzufuhr abgebrochen und sämtliche Schlauchverbindungen der Gasentwicklungsapparatur gelöst, um ein Zurücksaugen der Sperrflüssigkeiten zu vermeiden. 

Das Rohalkaloidgemisch wird über einen Büchner-Trichter abgesaugt 

10 mm3 der Mutterlauge werden mit 0,1 cm3 Chloroform verdünnt = DC-Lösung c.

Das Rohalkaloidgemisch (5 mg in 1,0 cm3 Methanol = DC-Lösung d) wird in einem 250 cm3 Becherglas mit ca. 30 cm3 Aceton versetzt, auf dem Wasserbad unter portionsweisem Zusatz von Methanol (ca. 10 cm3) erhitzt, bis es sich vollständig aufgelöst hat. 

Man gibt dann eine Spatelspitze Aktivkohle hinzu, kocht auf und filtriert. 

Zu dem Filtrat gibt man gerade so viel Aceton, bis das Ausfallen der Kristalle beginnt. 

Das beim Abkühlen nach längerer Zeit (6 h) ausgefallene kristalline Produkt wird über einen Glasfiltertrichter abgesaugt, mit wenig Aceton gewaschen und an der Luft getrocknet. 

Falls noch kein dc-einheitliches Produkt vorliegt, wird fraktioniert umkristallisiert; dazu wird das erhaltene Produkt in der 15fachen Menge Methanol gelöst, mit der 1OOfachen Menge Aceton versetzt und ca. 6 h bei 4° stehen gelassen. 

Die danach ausgefallenen Kristalle werden über einen Glasfiltertrichter abgesaugt. (Nach ein- bis zweimaligem Umkristallisieren wird ein dc-einheitliches Ephedrin-Hydrochlorid erhalten.)

Ausbeute: 0,5-1,5 g. Zeitbedarf: 2 Tage.

* http://www.chemieonline.de/forum/archive/index.php/t-93448.html
* source: https://erowid.org/archive/rhodium/chemistry/katze.txt
* mirror: https://archive.is/sJEbK
* screenshot: https://archive.is/sJEbK/80e94248b8556a74fdf5328c442d1958dbd61545/scr.png

The plant comes in 3 main varieties
* the 'wildtype' with 1.5% alkaloid content
* the 'preserved line' with 3%, 
* and the genetically engineered line with 8%  

Plus there is a shit load of commercial proucts promising more or less and plenty of bs...

Alcohol extracts on the raw plant will produce a a black, shiny (sometimes gooey) musty-smelling powder.  

It does react (as in the literature) to produce very low yields (<10%) (tested using Hypo and RP).  

Further refinements included the use of naptha to 'defat' the plant and sand filters to clean the extract. 

Both showed minimal improvements and added to the main burden of excess solvent usage.

A/B's on extracts (eg alcohol, water, HCL) did eventually produce clean results however difficulties arise from the mechanical losses from dealing with nasty emulsions.  

Additionally, pigments are other plant solubles are hard to remove.  

Lastly, freebase ephedrine's partial solubility in both water and NP solvents lowers yield. 

In nature, the alkaloids occur as polyhydrate structures that are hence weak acids or bases of water.  

That means that you need to convert them to a either a strong salt (using HCL)  or a strong freebase (using NaOH); in order to extract.  

The use of a blender and dry ice or freezing aids in the rupture of cell walls and increases the passage of alkaloids into solution.

Steam distilling can work well.  

Standard distillation of a pH 14 solution of the plant (or an extract) will liberate the alkaloids.  

Vacuum distillation didn't effect yield only speed things up.  

Foaming was a bitch as was the need to 'top-up' the volume of solution.

Steam distillation using an external steam source works better.  

This is because the freebase alkaloids (unlike meth) are better liberated by heating up the solution and plumbing in external steam (the Dwarjet works on the same principle).  

The distillate is then pH'd to 6 and all the water evaporated.  

An a/b of the distillate was discounted due to the large ammount of NP required and fb-ephedrines partial solubility conundrum.

When steam distilling, the main adulterant is 'Tannic Acid' (thanks Newton).  

It is difficult to remove and the best strategy was rinsing and decanting ice cold acetone, followed by a recrystalisation (more later).

Regardless of which route used, the goal (of efficiency) is to obtain clean white/clear alkaloid salts.  

Recrystalisations using alcohol/acetone were pretty good but the alcohol 'holds' onto the pseudo (lowering yields).  

Water/acetone works better and also better removes residual tannic acid contamination.  

The end result is odourless clear/white powder of even consistency and terrible bitter taste.  Now to react...

Reacting this (relative) pristine feedstock, it has been dreamed using hypo, rp and h3po3 using premade 57% HI as well as Iodine alone to produce HI insitu. 

Reaction yield derived from weight of feedstock varies depending on the post reaction workup scheme used - basic NP extract 70% down to 20% for the full optimum clean.

That is, a simple a/b on the reaction mix will extract the meth, but also other reduced and non-reacted alkaloids.  

It may all come out white and get u high; but it doesn't crystalise and tastes 'funny'.  

Gentle steam distillation (not using external steam) of the reaction mix better isolates the meth from unwanteds as they are better liberated using external forced steam distillation.  

The product can partially crystallise but mainly stays as 'powder'.  

For optimum results, an a/b can be performed on the distillate and the NP/meth-fb effectively washed.  

This evaporates down to a clear plate of glass.  It then can be slowly recrystalised by evaporating IPA to produce a majority of nicely sized clear crystals that look, crack and smoke like odourless taseless meth.

OBVIOUSLY, this is a fictitious account of one individual.

from http://chemistry.mdma.ch/hiveboard/crystal/000515979.html

-----

### archive.is - Ephedra's Role As a Precursor in the Clandestine Manufacture of Metha... 

* screenshot: https://archive.is/8D7nj/657060e263175d3b201860e7b9cd73e2cfd32a55/scr.png

#### how are you drying it?

put it in a pyrix dish in the oven until there's about 100ml of yelow liquid left. 

put it on the stove and heat gently from below. Try not to exceed 70c.

I've adjusted the ph from 5-6.5 to about 7.2. It works better and gives a longer lasting product.

also used 70% Medical alcohol instead of ISO Propynol.

Make sure you use commercial grade distilled water and not the crap you find at chemists.

#### alt.drugs.chemistry
* https://groups.google.com/d/msg/alt.drugs.chemistry/TB9P3gZSWW8/k-hcLS_Z3GsJ
* just to get a glimpse of the equipment that might be used for similar stuff: see https://archive.is/Vhbqg
* see http://imgur.com/a/A3621
* do not use battery acid: https://archive.is/2whyV

### Pseudoephedrine Acetate
* source: https://sbillinghurst.wordpress.com/
* mirror: https://archive.is/vp4NZ
