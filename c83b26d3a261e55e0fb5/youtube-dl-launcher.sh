#!/bin/bash

##
## $Log: youtube-dl-launcher.sh,v $
## Revision 1.12  2015/09/06 19:55:21  noke
## *** empty log message ***
##
## Revision 1.11  2015/08/13 19:29:21  noke
## *** empty log message ***
##
## Revision 1.10  2015/08/13 19:28:42  noke
## *** empty log message ***
##
##

YTDL_BIN="/home/noke/apps/youtube-dl"

YTDL_PARMS="-4 -i -c --no-part --socket-timeout 20 -R 5 --no-mtime"

YTDL_PARMS_MP3='--youtube-skip-dash-manifest -w --no-check-certificate --prefer-insecure -o "%(title)s.%(ext)s" -x --audio-format mp3 --audio-quality 3'

YTDL_PARMS_VIDEO="--prefer-ffmpeg"

if [ -x "$(which axel)" ]
  then 
    YTDL_PARAMS_EXTERNAL_DOWNLOADER="--external-downloader axel"
fi

PARAMLIST="${@}"

$YTDL_BIN --update

if [ "$1" == "mp3" ] 
    then 
        $YTDL_BIN ${YTDL_PARMS} ${YTDL_PARMS_MP3} ${PARAMLIST[@]:1}
    else 
        $YTDL_BIN ${YTDL_PARMS} ${YTDL_PARMS_VIDEO} ${YTDL_PARAMS_EXTERNAL_DOWNLOADER} ${PARAMLIST[@]} 
fi

