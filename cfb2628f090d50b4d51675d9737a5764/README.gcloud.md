## list instances

```
 gcloud compute instances list --filter=RUNNING --uri | sort
 
 gcloud compute instances list --format yaml --filter=RUNNING
 
 gcloud compute instances list --format yaml --filter=TERMINATED

 gcloud compute instances list --format yaml | grep -E '^(name|machineType|status): .*' | sed -r 's/machineType/\nmachineType/g;'

 gcloud compute instances list --filter=RUNNING --filter=haproxy,sshd
 
 ```